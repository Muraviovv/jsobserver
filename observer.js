
var ObserverClass = function()
{
    this.eventsParticipants = [];
};

ObserverClass.prototype.participate = function(event, callback, instance)
{
    if (!event) {throw Exeption("event should be a string");}

    var eventObject = {event:event, callback:callback, instance:instance};

    var a = this.eventsParticipants;

    var eventParticipantsArray = this.eo_getEvent(event, a);
    if (!eventParticipantsArray)
    {
        this.eventsParticipants[event] = [eventObject];

    } else {

        var index = eventParticipantsArray.indexOf(callback);
        if (index == -1)
        {
            this.eventsParticipants[event].push(eventObject);
        }
    }
};

ObserverClass.prototype.unsubscribe = function (event, callback)
{

    var eventParticipantsArray;
    var index;

    var a = this.eventsParticipants;
    if (event)
    {
        eventParticipantsArray = this.eo_getEvent(event, a);
        index = this.eo_getEventIndex(eventParticipantsArray, callback);
        if (index > -1)
        {
            eventParticipantsArray.splice(index, 1);
            this.eventsParticipants[event] = eventParticipantsArray;

            console.log("size:"+index, event, "unsubscribed");
        }


    } else
    {
        for (var key in a)
        {
            eventParticipantsArray = this.eo_getEvent(key,a);
            index = this.eo_getEventIndex(eventParticipantsArray, callback);

            if (index > -1)
            {
                eventParticipantsArray.splice(index, 1);
                this.eventsParticipants[event] = eventParticipantsArray;
            }
        }
    }
};

ObserverClass.prototype.removeInstance = function (instance)
{
    var eventParticipantsArray;
    var index;

    var a = this.eventsParticipants;
    if (!instance) {throw Exeption("Instance should be an object");}

    var indexes = [];
    for (var key in a)
    {
        var event = key;

        eventParticipantsArray = this.eo_getEvent(key, a);
        var count = eventParticipantsArray.length;
        for (var i=0; i<count; i++)
        {
            var eventObj = eventParticipantsArray[i];
            if (eventObj.instance == instance)
            {
                indexes.push(i);
            }
        }

        for (var key in indexes)
        {
            eventParticipantsArray.splice(indexes[key], 1);
            this.eventsParticipants[event] = eventParticipantsArray;
        }
    }
};

ObserverClass.prototype.postEvent = function(event, obj)
{
    var a = this.eventsParticipants;
    var eventParticipantsArray = this.eo_getEvent(event,a);
    for (var key in eventParticipantsArray)
    {
        eventParticipantsArray[key].callback({event:event, object:obj});
    }
};

ObserverClass.prototype.eo_getEvent = function(event,a)
{
    for (var key in a)
    { if (key == event) return a[key]; }
    return null;
};

ObserverClass.prototype.eo_getEventIndex = function(a, callback)
{
    var index = -1;
    var count = a.length;
    for (var i=0;i<count;i++)
    {
        index = i;
        if (a[i].callback == callback) return index;
    }
    return index;
};


window.Observer = new ObserverClass();